import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalService } from 'ng-zorro-antd/modal';
import { DateTime } from 'luxon';
import { AxiosResponse } from 'axios';
import { LibService } from '../../../shared/services/lib.service';
import { UserProfileService } from '../../../core/services/user-profiles.service';
import * as _ from 'lodash';
@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent {
  wardId: any;
  wards: any = [];
  query: any = '';
  dataSet: any[] = [];
  loading = false;

  total = 0;
  pageSize = 20;
  pageIndex = 1;
  offset = 0;
  user_login_name: any;
  constructor(
    private router: Router,
    private libService: LibService,
    private message: NzMessageService,
    private modal: NzModalService,
    private userProfileService: UserProfileService
  ) {

  }

  async ngOnInit() {
    // this.user_login_name  =  this.userProfileService.user_login_name;  
    this.user_login_name  =  sessionStorage.getItem('userLoginName'); 
   
    console.log(this.user_login_name);

  }

  async getList() {

  }

  logOut() {
    sessionStorage.setItem('token', '');
    return this.router.navigate(['/login']);
  }


  onPageIndexChange(pageIndex: any) {

    this.offset = pageIndex === 1 ?
      0 : (pageIndex - 1) * this.pageSize;

    this.getList()
  }

  onPageSizeChange(pageSize: any) {
    this.pageSize = pageSize
    this.pageIndex = 1

    this.offset = 0

    this.getList()
  }

  refresh() {
    this.query = '';
    this.pageIndex = 1;
    this.offset = 0;
    this.getList();
  }
}
