import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DoctorRoutingModule } from './doctor-routing.module';
import { DoctorComponent } from './doctor.component';
import { NgZorroModule } from 'src/app/ng-zorro.module';
import { SharedModule } from '../../shared/shared.module';


@NgModule({
  declarations: [
    DoctorComponent
  ],
  imports: [
    CommonModule,
    NgZorroModule,
    SharedModule,
    DoctorRoutingModule
  ]
})
export class DoctorModule { }
